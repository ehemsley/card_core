module CardCore
  class DummyCribbageCore < BaseCribbageCore
    def self.name
      "Dummy"
    end

    def self.discard_choices(legal_moves)
      legal_moves.first(2)
    end

    def self.peg_choice(legal_moves, peg_card_list)
      legal_moves.first
    end
  end
end
