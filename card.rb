module CardCore
  class Card
    SUCC_VALUES = {
                    ace: :two,
                    two: :three,
                    three: :four,
                    four: :five,
                    five: :six,
                    six: :seven,
                    seven: :eight,
                    eight: :nine,
                    nine: :ten,
                    ten: :jack,
                    jack: :queen,
                    queen: :king
    }

    SUIT_PLURALS = {
                      heart: 'hearts',
                      diamond: 'diamonds',
                      spade: 'spades',
                      club: 'clubs'
    }

    VALUE_PLURALS = {
                      ace: 'aces',
                      two: 'twos',
                      three: 'threes',
                      four: 'fours',
                      five: 'fives',
                      six: 'sixes',
                      seven: 'sevens',
                      eight: 'eights',
                      nine: 'nines',
                      ten: 'tens',
                      jack: 'jacks',
                      queen: 'queens',
                      king: 'kings'
    }

    attr_accessor :suit, :value
    def initialize(suit, value)
      @suit = suit
      @value = value
    end

    def succ_value
      SUCC_VALUES[@value]
    end

    def pluralize_value
      VALUE_PLURALS[@value]
    end

    def to_s
      "#{@value.to_s} of #{SUIT_PLURALS[@suit]}"
    end

    def self.pluralize_value(value)
      VALUE_PLURALS[value]
    end
  end
end
