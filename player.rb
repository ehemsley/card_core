module CardCore
  class Player
    attr_accessor :cards #this probably shouldnt be public?
    attr_accessor :played_cards
    attr_accessor :ai #i dont like this pattern, i feel like this shouldnt be public
    attr_accessor :name

    def initialize(ai_core_class, name)
      @cards = []
      @played_cards = []

      @ai = ai_core_class
      @name = name
    end

    def empty_hand?
      @cards.empty?
    end

    def add_card(card)
      @cards << card
    end

    def discard(card)
      @cards.delete(card)
    end

    def play_card(card)
      @cards.delete(card)
      @played_cards << card
    end

    def return_played_cards_to_hand
      @played_cards.each { |card| @cards << card }
      @played_cards.clear
    end

    def clear_hand
      @cards.clear
      @played_cards.clear
    end
  end
end
