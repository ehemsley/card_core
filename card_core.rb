require './card'
require './deck'
require './player'
require './game'
require './games/cribbage'
require './ai_core'
require './ai_cores/cribbage/base_cribbage_core'
require './ai_cores/cribbage/dummy_cribbage_core'
require './ai_cores/cribbage/cryptage_cribbage_core'

module CardCore
  SUITS = [:heart, :spade, :diamond, :club]
  VALUES = [:ace, :two, :three, :four, :five, :six, :seven, :eight, :nine, :ten, :jack, :queen, :king]
end
