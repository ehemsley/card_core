require 'spec_helper'

describe CardCore::Deck do
  let(:klass) { CardCore::Deck }
  let(:deck) { CardCore::Deck.new }

  describe 'self.standard_cards' do
    it 'should have 52 cards' do
      expect(klass.standard_cards.count).to eql(52)
    end
  end

  describe 'draw' do
    it 'returns the top card' do
      top_card = deck.instance_variable_get(:@cards).first
      expect(deck.draw).to eql(top_card)
    end

    it 'removes the top card from the deck' do
      top_card = deck.instance_variable_get(:@cards).first
      deck.draw
      expect(top_card).to_not eql(deck.instance_variable_get(:@cards).first)
    end
  end

  describe 'add_card_to_bottom' do
    let(:card) { CardCore::Card.new(:heart, :jack) }
    it 'adds given card to the bottom of the deck' do
      deck.add_to_bottom(card)
      expect(deck.instance_variable_get(:@cards).last).to eql(card)
    end
  end

  describe 'add_cards_to_bottom' do
    let(:cards) { [CardCore::Card.new(:heart, :jack), CardCore::Card.new(:spade, :jack)] }
    it 'adds multiple cards to the bottom of the deck' do
      deck.add_cards_to_bottom(cards)
      expect(deck.instance_variable_get(:@cards).last(2)).to eql(cards)
    end
  end

  describe 'remaining_cards_count' do
    it 'counts the remaining cards' do
      deck.draw
      expect(deck.remaining_cards_count).to eql(51)
    end
  end
end
