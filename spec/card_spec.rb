require 'spec_helper'

describe CardCore::Card do
  let(:card) { CardCore::Card.new(:heart, :jack) }

  describe 'succ_value' do
    it 'returns the successor value' do
      expect(card.succ_value).to eql(:queen)
    end
  end

  describe 'pluralize_value' do
    it 'returns the pluralized value' do
      expect(card.pluralize_value).to eql('jacks')
    end
  end

  describe 'to_s' do
    it 'displays itself as a string' do
      expect(card.to_s).to eql('jack of hearts')
    end
  end
end
