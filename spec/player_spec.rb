require 'spec_helper'

describe CardCore::Player do
  let(:ai_klass) { class_double('AiCore') }
  let(:player) { CardCore::Player.new(ai_klass, '') }
  let(:card) { CardCore::Card.new(:heart, :jack) }

  describe 'empty_hand?' do
    context 'hand is empty' do
      it 'returns true' do
        expect(player.empty_hand?).to eql(true)
      end
    end

    context 'hand is not empty' do
      it 'returns false' do
        player.add_card(card)
        expect(player.empty_hand?).to eql(false)
      end
    end
  end

  describe 'add_card' do
    it 'adds card to hand' do
      player.add_card(card)
      expect(player.cards).to include(card)
    end
  end

  describe 'discard' do
    it 'removes card from hand' do
      player.add_card(card)
      player.discard(card)
      expect(player.cards).to_not include(card)
    end
  end

  describe 'play_card' do
    before(:each) do
      player.add_card(card)
      player.play_card(card)
    end

    it 'removes card from hand' do
      expect(player.cards).to_not include(card)
    end

    it 'adds card to list of played cards' do
      expect(player.played_cards).to include(card)
    end
  end

  describe 'return_played_cards_to_hand' do
    let(:card_two) { CardCore::Card.new(:heart, :queen) }

    before(:each) do
      player.add_card(card)
      player.add_card(card_two)
      player.play_card(card)
      player.play_card(card_two)
      player.return_played_cards_to_hand
    end

    it 'puts played cards back in hand' do
      expect(player.cards).to include(card, card_two)
    end

    it 'removes cards from list of played cards' do
      expect(player.played_cards).to be_empty
    end
  end

  describe 'clear_hand' do
    before(:each) do
      3.times { player.add_card(card) }
      player.clear_hand
    end

    it 'empties hand' do
      expect(player.cards).to be_empty
    end

    it 'removes cards from played_cards' do
      expect(player.played_cards).to be_empty
    end
  end
end
